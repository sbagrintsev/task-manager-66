package ru.tsc.bagrintsev.tm.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.bagrintsev.tm.api.sevice.IReceiverService;

import javax.jms.*;

@Service
@RequiredArgsConstructor
public class ReceiverService implements IReceiverService {

    @NotNull
    private static final String QUEUE = "TM_QUEUE";

    @NotNull
    private final ConnectionFactory connectionFactory;

    @Override
    @SneakyThrows
    public void receive(@NotNull final MessageListener listener) {
        final Connection connection = connectionFactory.createConnection();
        connection.start();
        final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        final Destination destination = session.createQueue(QUEUE);
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(listener);
    }

}
