package ru.tsc.bagrintsev.tm.api.sevice;

public interface ILoggerService {

    void logToConsole(String json);

    void logToFile(String json);

    void logToMongo(String json);

}
