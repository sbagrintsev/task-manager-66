package ru.tsc.bagrintsev.tm.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.bagrintsev.tm.model.AbstractUserOwnedModel;

@Repository
@Scope("prototype")
public interface AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> {

}
