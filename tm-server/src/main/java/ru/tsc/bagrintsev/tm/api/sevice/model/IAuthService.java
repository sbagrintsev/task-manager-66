package ru.tsc.bagrintsev.tm.api.sevice.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.user.AccessDeniedException;
import ru.tsc.bagrintsev.tm.model.Session;

import java.security.GeneralSecurityException;

public interface IAuthService {

    void clearAll();

    @NotNull
    String signIn(
            @Nullable String login,
            @Nullable String password
    ) throws AbstractException, JsonProcessingException, GeneralSecurityException;

    @NotNull
    Session validateToken(@Nullable String token) throws AccessDeniedException, JsonProcessingException;

}
