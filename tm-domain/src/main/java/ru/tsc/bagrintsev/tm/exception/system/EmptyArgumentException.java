package ru.tsc.bagrintsev.tm.exception.system;

import ru.tsc.bagrintsev.tm.exception.AbstractException;

public final class EmptyArgumentException extends AbstractException {

    public EmptyArgumentException() {
        super("Error! Argument is empty");
    }

}
