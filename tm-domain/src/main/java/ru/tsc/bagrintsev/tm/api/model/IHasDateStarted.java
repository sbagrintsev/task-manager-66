package ru.tsc.bagrintsev.tm.api.model;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasDateStarted {

    @Nullable
    Date getDateStarted();

    void setDateStarted(@Nullable final Date dateStarted);

}
