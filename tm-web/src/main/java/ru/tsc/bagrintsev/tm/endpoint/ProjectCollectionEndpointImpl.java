package ru.tsc.bagrintsev.tm.endpoint;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.api.endpoint.ProjectCollectionRestEndpoint;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.service.ProjectService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/projects")
public class ProjectCollectionEndpointImpl implements ProjectCollectionRestEndpoint {

    private final ProjectService projectService;

    @PostMapping
    public void save(@RequestBody List<Project> projects) {
        projectService.saveAll(projects);
    }

    @DeleteMapping
    public void delete(@RequestBody List<String> projectIds) {
        projectService.deleteAll(projectIds);
    }

    @GetMapping
    public List<Project> get() {
        return projectService.findAll();
    }

}
