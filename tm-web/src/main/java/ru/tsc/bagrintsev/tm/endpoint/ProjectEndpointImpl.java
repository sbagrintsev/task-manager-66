package ru.tsc.bagrintsev.tm.endpoint;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.api.endpoint.ProjectRestEndpoint;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.service.ProjectService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/project")
public class ProjectEndpointImpl implements ProjectRestEndpoint {

    private final ProjectService projectService;

    @PostMapping
    public void save(@RequestBody Project project) {
        projectService.save(project);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") String id) {
        projectService.deleteById(id);
    }

    @GetMapping("/{id}")
    public Project get(@PathVariable("id") String id) {
        return projectService.findById(id);
    }

}
