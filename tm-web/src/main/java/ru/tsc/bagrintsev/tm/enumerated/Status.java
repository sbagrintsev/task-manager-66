package ru.tsc.bagrintsev.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    public final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

}
