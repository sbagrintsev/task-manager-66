package ru.tsc.bagrintsev.tm.endpoint;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.api.endpoint.TaskRestEndpoint;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.service.ProjectService;
import ru.tsc.bagrintsev.tm.service.TaskService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/task")
public class TaskEndpointImpl implements TaskRestEndpoint {

    private final TaskService taskService;

    @PostMapping
    public void save(@RequestBody Task task) {
        taskService.save(task);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") String id) {
        taskService.deleteById(id);
    }

    @GetMapping("/{id}")
    public Task get(@PathVariable("id") String id) {
        return taskService.findById(id);
    }

}
