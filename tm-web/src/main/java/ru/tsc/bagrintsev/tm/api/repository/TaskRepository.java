package ru.tsc.bagrintsev.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.bagrintsev.tm.model.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

}
