package ru.tsc.bagrintsev.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.model.Project;

import java.util.List;

public interface ProjectCollectionRestEndpoint {

    @PostMapping(produces = "application/json")
    void save(@RequestBody List<Project> projects);

    @DeleteMapping(produces = "application/json")
    void delete(@RequestBody List<String> projectIds);

    @GetMapping(produces = "application/json")
    List<Project> get();

}
