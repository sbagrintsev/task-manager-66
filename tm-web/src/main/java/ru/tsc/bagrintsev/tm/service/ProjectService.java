package ru.tsc.bagrintsev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.api.repository.ProjectRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProjectService {

    private final ProjectRepository projectRepository;

    @Transactional
    public void deleteAll(@NotNull final List<String> projectIds) {
        projectRepository.deleteAllById(projectIds);
    }

    @Transactional
    public void deleteById(@NotNull final String id) {
        projectRepository.deleteById(id);
    }

    @NotNull
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        return projectRepository.findById(id).orElse(null);
    }

    @Transactional
    public void save(@NotNull final Project project) {
        projectRepository.save(project);
    }

    @Transactional
    public void saveAll(@NotNull final List<Project> projects) {
        projectRepository.saveAll(projects);
    }

}
