package ru.tsc.bagrintsev.tm.endpoint;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.api.endpoint.TaskCollectionRestEndpoint;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.service.ProjectService;
import ru.tsc.bagrintsev.tm.service.TaskService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/tasks")
public class TaskCollectionEndpointImpl implements TaskCollectionRestEndpoint {

    private final TaskService taskService;

    @PostMapping
    public void save(@RequestBody List<Task> tasks) {
        taskService.saveAll(tasks);
    }

    @DeleteMapping
    public void delete(@RequestBody List<String> taskIds) {
        taskService.deleteAll(taskIds);
    }

    @GetMapping
    public List<Task> get() {
        return taskService.findAll();
    }

}
