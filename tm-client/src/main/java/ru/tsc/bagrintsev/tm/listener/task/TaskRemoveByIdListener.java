package ru.tsc.bagrintsev.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskRemoveByIdRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

@Component
public class TaskRemoveByIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String description() {
        return "Remove task by id.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@taskRemoveByIdListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        showParameterInfo(EntityField.ID);
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(getToken());
        request.setTaskId(id);
        taskEndpoint.removeTaskById(request);
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove";
    }

}
