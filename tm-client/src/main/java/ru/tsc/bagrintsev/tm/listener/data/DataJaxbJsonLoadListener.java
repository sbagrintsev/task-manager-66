package ru.tsc.bagrintsev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJaxbJsonLoadRequest;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public final class DataJaxbJsonLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-jaxb-json";

    @NotNull
    @Override
    public String description() {
        return "Load current application state from json file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJaxbJsonLoadListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        domainEndpoint.loadJaxbJson(new DataJaxbJsonLoadRequest(getToken()));
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
