package ru.tsc.bagrintsev.tm.listener.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.bagrintsev.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.listener.AbstractListener;

@NoArgsConstructor
public abstract class AbstractDataListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpoint;

    @Override
    public @NotNull Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public @NotNull String shortName() {
        return "";
    }

}
