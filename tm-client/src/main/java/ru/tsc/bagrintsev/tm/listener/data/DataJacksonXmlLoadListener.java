package ru.tsc.bagrintsev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJacksonXmlLoadRequest;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public final class DataJacksonXmlLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-xml";

    @NotNull
    @Override
    public String description() {
        return "Load current application state from xml file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJacksonXmlLoadListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        domainEndpoint.loadJacksonXml(new DataJacksonXmlLoadRequest(getToken()));
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
