package ru.tsc.bagrintsev.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.model.TaskDto;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskShowByIdRequest;
import ru.tsc.bagrintsev.tm.dto.response.task.TaskShowByIdResponse;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

@Component
public class TaskShowByIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String description() {
        return "Show task by id.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@taskShowByIdListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        showParameterInfo(EntityField.ID);
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(getToken());
        request.setTaskId(id);
        @Nullable final TaskShowByIdResponse response = taskEndpoint.showTaskById(request);
        @Nullable final TaskDto task = response.getTask();
        if (task != null) {
            showTask(task);
        }
    }

    @NotNull
    @Override
    public String name() {
        return "task-show";
    }

}
